import React from 'react'
import { Table } from 'semantic-ui-react'
import axios from 'axios'

class TableProducts extends React.Component {
  
  state = {
    tabledata: []
  }    
  componentDidMount()
  {
    axios.get("http://localhost:8000/api/ofertas")
    .then(res => {
        const tabledata = res.data;
        this.setState({tabledata});
    })
  }
  render() {
    const headerRow = ['Producto', 'Compra minima', 'Compra maxima','Precio','Telefono','Nombre','Descripcion']
  
    const renderBodyRow = ({ id_producto, minimo, maximo, precio, telefono, vendedor, descripcion }, i) => ({
      
      cells: [
        id_producto,
        minimo,
        maximo,
        precio,
        telefono,
        vendedor,
        descripcion
      ],
    })
  
    return (
    <Table
      celled
      headerRow={headerRow}
      renderBodyRow={renderBodyRow}
      tableData={this.state.tabledata}
    />
      )
      }
  
}
  export default TableProducts