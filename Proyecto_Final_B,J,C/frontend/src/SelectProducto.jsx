import React from 'react'
import { Select } from 'semantic-ui-react'
import axios from 'axios'

class SelectProducto extends React.Component
{
    constructor(props)
    {
        super(props);
    }
state = {
    opciones: []
}    
componentDidMount()
{
    axios.get("http://localhost:8000/api/productos")
    .then(res => {
        const opciones = res.data;
        this.setState({opciones});
    })
}

render() {
    const opciones = []
    const data = this.state.opciones;
    for (let i=0;i<data.length;i++)
        opciones.push({ key: data[i].id_producto, value: data[i].id_producto, text: data[i].descripcion })
    return (
 /*       <ul>
            { this.state.opciones.map(option => <li>{ option.descripcion }</li>) }
        </ul>*/
    <Select compact id="producto" name="id_producto" options={opciones} />
    
    )    
}

}  
export default SelectProducto
