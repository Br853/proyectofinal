import React from 'react'
import { Button, Form, Input, Select } from 'semantic-ui-react'
import axios from 'axios'
class FormProduct extends React.Component 
{
    constructor(props)
    {
        super(props);
    }
state = {
    oferta: {},
    opciones: []
 }    
 componentDidMount()
 {
     axios.get("http://localhost:8000/api/productos")
     .then(res => {
         const opciones = res.data;
         this.setState({opciones});
     })
 }

handleSubmit = (event) => {
    event.preventDefault();
    const oferta= this.state.oferta;
    axios.post("http://localhost:8000/api/ofertas",{oferta})
    .then(res => {
        console.log(res.data);
    })
}
handleChange = (event) => {
    let oferta=this.state.oferta;
    let name=event.target.name;
    let value=event.target.value;
    oferta[name]=value;
    this.setState({oferta})
    console.log(this.state);
} 

handleChangeSelect = (event,data) => {
    let oferta=this.state.oferta;
    let name=data.name;
    let value=data.value;
    oferta[name]=value;
    this.setState({oferta})
    console.log(this.state);
} 
render() {
    const opciones = []
    const data = this.state.opciones;
    for (let i=0;i<data.length;i++)
        opciones.push({ key: data[i].id_producto, value: data[i].id_producto, text: data[i].descripcion })
  
    return (
  <Form>
    <Form.Field>
      <label>Producto</label>
      <Select compact name="id_producto" options={opciones} onChange={this.handleChangeSelect}  />         
    </Form.Field>
    <Form.Field>
      <label>Compra minima</label>
      <Input name="minimo" placeholder='minimo en libras' onChange={this.handleChange} />
    </Form.Field>
    <Form.Field>
      <label>Compra maximo</label>
      <Input name="maximo" placeholder='maximo en libras' onChange={this.handleChange}  />
    </Form.Field>
    <Form.Field>
      <label>Precio</label>
      <Input name="precio" placeholder='Precio por libra' onChange={this.handleChange} />
    </Form.Field>
    <Form.Field>
      <label>Telefono</label>
      <Input name="telefono" placeholder='telefono de contacto' onChange={this.handleChange}  />
    </Form.Field>
    <Form.Field>
      <label>Nombre</label>
      <Input name="vendedor" placeholder='Su nombre' onChange={this.handleChange} />
    </Form.Field>
    <Form.Field>
      <label>Descripcion</label>
      <Input name="descripcion" placeholder='descripcion' onChange={this.handleChange} />
    </Form.Field>

    <Button type='submit' onClick={this.handleSubmit}>Agregar</Button>
  </Form>
    )
    }

}
export default FormProduct