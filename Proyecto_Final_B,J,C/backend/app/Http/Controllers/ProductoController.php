<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
/**
* @OA\Info(
*     title="My the best API",
*     version="1.0.0")
*/
class ProductoController extends Controller
{
     /**  @OA\Get(
     *      path="/api/productos",
     *      operationId="listaProducto",
     *      summary="Obtener lista de productos",
     *      description="Devuelve la lista de los productos",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */

    public function list()
    {

        return Producto::all();
    }
}
