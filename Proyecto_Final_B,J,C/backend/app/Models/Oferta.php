<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    use HasFactory;
    protected $table="oferta";
    protected $primaryKey="id_oferta";
    public $incrementing = true;
    public $timestamps = false;
    protected $fillable=["descripcion","minimo","maximo","precio","id_producto","telefono","vendedor","id_producto"];

}
