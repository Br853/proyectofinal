<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reserva extends Model
{
    use HasFactory;
    protected $table="reservas";
    protected $primaryKey="reserva";
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable=['fecha_creacion','fecha','hora','placa','estado','cedula','reserva'];
    
}
