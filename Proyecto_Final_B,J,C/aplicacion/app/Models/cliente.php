<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    use HasFactory;
    protected $table="clientes";
    protected $primaryKey="cedula"
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['cedula','nombre','telefono']
}
