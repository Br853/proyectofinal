
@section("contenido")
<!DOCTYPE html>
<html>
    <head>
        <title>Title</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.22/dist/css/uikit.min.css" />

        <!-- UIkit JS -->
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.22/dist/js/uikit.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.22/dist/js/uikit-icons.min.js"></script>
    </head>
    <body>
       <div class="uk-container uk-container-small">
            <div class="uk-card">
               <div class="uk-card-header">
                    <h3 class="uk-center">Reservar</h3>
               </div>
               
             <form action="{{route('guardar')}}" method="post">@csrf
                 Cedula <input class="uk-input" name="cedula" type="text"><br>
                 Nombre <input class="uk-input" name="nombre" type="text"><br>
                 Placa <input class="uk-input" name="Placa" type="text"><br>
                 Telefono <input name="telefono" class="uk-input" type="text">
                 <br>
                 <br>
                 
                 <select class="uk-select" name="reserva">
                    @foreach($reservas as $reserva)
                    <option value="{{$reserva->reserva}}">{{$reserva->fecha}}/{{$reserva->hora}}</option>
                    @endforeach
                
                 </select><br>
                 <br><input class="uk-button uk-button-primary" type="submit">
             </form>
          </div>
       </div>
    </body>
</html>










